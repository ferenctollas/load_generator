Rhino: Tool for performance tests, stress and stability tests
===============================================================
Author
========
Ferenc Tollas

Version
========
0.0.6
Description
============
Rhino is a simple console based application, which can be used to run performance,
stability and stress tests. Rhino is a framework which provides services to easily 
implement and run clients which can be used for non functional testing.
See the documentation for more details.

First generate the default configuration file:
python3 -m rhino -init > <configuration file>

To run controller:
python3 -m rhino --mode server -lp 8888 --config <configuration file>

To run an agent:
python3 -m rhino --name testagent --mode client -lp 8889 -ci 127.0.0.1 -cp 8888 --config  <configuration file>
