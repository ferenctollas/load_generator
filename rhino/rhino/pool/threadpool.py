from threading import Thread
import time
from ..util.util import log_info, log_error, Context, ClientQueue, my_import
from ..msg.msgfactory import SetTestConfigMessage
from ..util.util import interface, implements


@interface
class ThreadPoolInterface:

    def initialise(self, with_test_client_name: str, with_test_config: SetTestConfigMessage=None, with_thread_number=1):
        pass

    def start_executors(self):
        pass

    def stop_executors(self):
        pass


@implements(ThreadPoolInterface)
class ThreadPool:

    @property
    def is_running(self):
        return self._running

    @property
    def thread_number(self):
        return self._thread_number

    @property
    def test_configuration(self):
        return self._test_config

    @property
    def client_name(self):
        return self._test_client_name

    def __init__(self, thread_number=1, client_queue: ClientQueue=None):
        self._thread_number = thread_number
        self._threads = []
        self._running = False
        self._test_config = None
        self._test_client_name = None
        self._context = Context(client_queue)

    def initialise(self, with_test_client_name: str, with_test_config: SetTestConfigMessage=None, with_thread_number=1):
        self._thread_number = with_thread_number
        self._test_config = with_test_config
        self._test_client_name = with_test_client_name

    def _create_threads(self):
        self._threads = [ExecutorThread(test_client=my_import(self._test_client_name)(),
                                        test_configuration=self._test_config,
                                        context=self._context)
                         for _ in range(self._thread_number)
                         ]

    def start_executors(self):
        if self._test_client_name is None:
            log_error('ThreadPool] No client name was given.')
            return

        if self._test_config is None:
            log_error('[ThreadPool] No configuration was given')
            return
        self._create_threads()
        for i in range(self._thread_number):
            self._threads[i].start()
        self._running = True

    def stop_executors(self):
        if len(self._threads) == 0:
            return

        for i in range(0, self._thread_number):
            self._threads[i].terminate()
        self._running = False


class ExecutorThread(Thread):

    def __init__(self, test_client=None, test_configuration: SetTestConfigMessage=None, context: Context=None):
        super().__init__()
        self._context = context
        self._test_client = test_client
        self._test_configuration = test_configuration
        self._test_client.initialise(self._test_configuration.parameters, self._context)
        self._terminate = False

    def __del__(self):
        if self._test_client:
            self._test_client.uninitialise()

    def run(self):
        if self._test_configuration is None:
            log_error('[ThreadPool]Configuration is None!')
            return

        log_info('Thread pool begin to start.')
        if self._test_configuration.test_type == 'INFINITE':
            self._run_infinite_test()

        elif self._test_configuration.test_type == 'SEQUENCE':
            self._run_test_once()

        elif self._test_configuration.test_type == 'TIME_LIMITED':
            self._run_time_limited_test()

    def _run_infinite_test(self):
        log_info('[ThreadPool] Running INFINITE configuration')
        while self._terminate is False:
            self._run_test_once()
        pass

    def _run_time_limited_test(self):
        log_info('[ThreadPool] Running TIME_LIMITED configuration')
        _time_to_run = self._test_configuration.test_time * 1000
        start_time = time.time()
        while (time.time() - start_time) < _time_to_run:
            self._run_test_once()
        log_info('[ThreadPool] Running TIME_LIMITED configuration, DONE')

    def _run_test_once(self):
        log_info('[ThreadPool] Running SEQUENCE configuration')
        test_methods = self._test_configuration.test_methods
        for i in range(len(test_methods)):
            func = getattr(self._test_client, test_methods[i]['method_name'])
            for execution_number in range(test_methods[i]['method_execution_number']):
                try:
                    if len(test_methods[i]['method_params']) == 0:
                        func()
                    else:
                        log_info('[ThreadPool] Call method with params {}'.format(str(test_methods[i]['method_params'])))
                        func(*test_methods[i]['method_params'])
                except Exception as exception:
                    log_error(str(exception))

    def terminate(self):
        self._terminate = True
