from argparse import ArgumentParser
import sys
from string import Template
from ..util.util import get_clients_methods, my_import

config_template = Template("""
    {
     "command": "set_test_config",
     "simulate_client_number": 1,
     "client_name": "$CLIENT_NAME",
     "test_type": "SEQUENCE",
     "time":2345,
     "parameters": {
            "parameter1":"value1"
     },
     "test_methods": [
        $METHODS
     ]
    }
""")

method_template = Template("""
         {
             "method_name": "$METHOD_NAME",
             "method_execution_number": 1,
             "method_params": [
                 $PARAMETERS
             ]
         }
""")


def write_out_configuration(client_name: str):
    if client_name is None or client_name.strip() == '':
        print("Type help for parameters.")
        return
    methods = get_clients_methods([client_name])
    method_data = methods[client_name]
    method_data_length = len(method_data) - 1
    method_string = ''
    for idx, method in enumerate(method_data):
        method_string += method_template.substitute(METHOD_NAME=method[0],
                                                    PARAMETERS=','.join(
                                                        repr(value).replace("'", "\"") for value in method[2:]))
        if idx < method_data_length:
            method_string += ','

    final_string = config_template.substitute(CLIENT_NAME=client_name, METHODS=method_string)
    print(final_string)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-c', '--client', dest='client', help='Name of the client')
    args = parser.parse_args()
    if not args.client:
        print('Missing parameter')
        sys.exit(-1)
    write_out_configuration(args.client)
