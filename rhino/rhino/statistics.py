from time import sleep
from threading import Thread
from .msg.msgfactory import *
from .agents.agent  import ServerAgent


class Statistics(Thread):

    def __init__(self, server_agent: ServerAgent, sleep_seconds: int=2):
        super().__init__()
        self._server_agent = server_agent
        self._terminate = False
        self._sleep_seconds = sleep_seconds

    def set_interval(self, interval: int):
        self._sleep_seconds = interval

    def _broadcast_message(self, command: Message=None):
        if command:
            self._server_agent.broadcast_message_to_agents(message=command.toJson())

    def run(self):
        # get the statistics from the agents
        stat_cmd = StatisticsMessage()
        while not self._terminate:
            self._broadcast_message(stat_cmd)
            sleep(self._sleep_seconds)

    def terminate(self):
        self._terminate = True
