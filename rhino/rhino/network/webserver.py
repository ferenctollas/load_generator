from ..util.util import log_info, log_error, implements
from ..network.network import MessageListener
from ..msg.msgfactory import MsgFactory, Message, GetConnectedAgentsMessage
import os
import mimetypes
import urllib.parse
from http.server import BaseHTTPRequestHandler, HTTPServer
from time import sleep

from threading import Thread

shutdown_all = False
global_server_agent = None
web_directory = "www"


class RhinoHttpServer(Thread):

    def __init__(self, port_number: str, directory: str):
        super().__init__()
        self._port_number = port_number
        global web_directory
        if directory.endswith('/'):
            web_directory = directory[:-1]
        else:
            web_directory= directory
        log_info('Web server initialized with port %s' % self._port_number)
        self._server = None
        self._server_agent = None

    def run(self):
        log_info('Server starting')
        self._server = HTTPServer(('', self._port_number), RhinoHTTPHandler, bind_and_activate=True)
        self._server.serve_forever()

    def set_server_agent(self, server_agent):
        self._server_agent = server_agent
        global global_server_agent
        global_server_agent = self._server_agent

    def shutdown(self):
        log_info('Web servershutdown...')
        global shutdown_all
        shutdown_all = True
        self._server.shutdown()
        log_info('Web servershutdown...DONE')


class WebMessageProcessor:
    """
    Parse the body and use the ServerAgent to get the requested information
    """
    @staticmethod
    def parse_message(message: None) -> Message:
        """
        In case of async messages, the method returns None, the message
        will be send in async mode when the agents sends the message
        """
        log_info('Received post: %s' % message)
        log_info('Parsing message...')
        msg = MsgFactory.msg(message.decode())
        log_info('Parsing message...DONE')
        log_info('parsed message type  %s' % str(msg.type))

        if msg.type == Message.GET_CONNECTED_AGENTS:
            agents = global_server_agent.get_connected_clients()
            return GetConnectedAgentsMessage(agents)

        elif msg.type == Message.CLIENT_INFO:
            global_server_agent.send(msg.toJson(), to_agent=msg.agent_name)

        elif msg.type == Message.SET_TEST_CONFIGURATION:
            if msg.agent_name:
                global_server_agent.send(msg.toJson(), to_agent=msg.agent_name)
            elif msg.group_name:
                group_members = global_server_agent.get_group_members(msg.group_name)
                for member in group_members:
                    global_server_agent.send(msg.toJson(), to_agent=member)

        elif msg.type == Message.START:
            if msg.agent_name:
                global_server_agent.send(msg.toJson(), to_agent=msg.agent_name)
            elif msg.group_name:
                group_members = global_server_agent.get_group_members(msg.group_name)
                for member in group_members:
                    global_server_agent.send(msg.toJson(), to_agent=member)

        elif msg.type == Message.STOP:
            if msg.agent_name:
                global_server_agent.send(msg.toJson(), to_agent=msg.agent_name)
            elif msg.group_name:
                group_members = global_server_agent.get_group_members(msg.group_name)
                for member in group_members:
                    global_server_agent.send(msg.toJson(), to_agent=member)

        elif msg.type == Message.STATISTICS:
            global_server_agent.send(msg.toJson(), to_agent=msg.agent_name)

        return None


def load_file_content(path: str) -> str:
    if not os.path.isfile(path):
        raise ValueError(path)
    with open(path, 'rb') as file:
        return file.read()


@implements( MessageListener)
class PushMessageHandler:
    """
    All async messages will be received by this handler.
    When a message is received from the agents, it is
    forwarded to the UI if there is a listener.
    """

    def __init__(self, http_handler):
        self._http_handler = http_handler

    def arrived(self, message: str=None):
        log_info('[PushMessageHandler] Message received from agents %s' % message)
        if self._http_handler:
            self._http_handlerself.write_push_message(message)


class RhinoHTTPHandler(BaseHTTPRequestHandler):
    """
    Special urls:
    /push : register a listener thread to receive async messages from agents
    /api  : send message to the agents
    /     : loads the UI
    """
    def _handle_push(self) -> None:
        log_info('Push request handling started')
        msg_handler = PushMessageHandler(self)
        global_server_agent.register_message_listener(msg_handler)
        log_info('Registering message handling for async response...')
        while not shutdown_all:
            sleep(1)

    def do_POST(self):
        log_info('Post message to url %s' % self.path[1:])
        if self.path[1:] == 'api':
            length = int(self.headers['content-length'])
            content = self.rfile.read(length)
            log_info('POST %s' % content)
            msg = WebMessageProcessor.parse_message(content)
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            if msg:
                self.wfile.write(bytes(msg.toJson(), 'utf-8'))
                log_info('Response has been sent %s' % msg.toJson())
            else:
                self.wfile.write('', 'utf-8')
            self.wfile.flush()
        elif self.path [1:]== 'push':
            self._handle_push()

    def do_GET(self):
        log_info("Request to %s" % self.path)
        url = urllib.parse.urlparse(self.path)
        full_file_path = url.path
        if url.path == '/':
            full_file_path = web_directory + '/index.html'

        elif self.path[1:] == 'push':
            self._handle_push()

        else:
            full_file_path = web_directory + url.path
            log_info('Full path %s' % full_file_path)
        try:
            content = load_file_content(full_file_path)
            mimetype, _ = mimetypes.guess_type(full_file_path)
            self._send_response(200, mimetype, content)
            log_info('Response has been set %s' % content)
        except ValueError as ve:
            log_error(str(ve))
            self._send_error_message()

    def write_push_message(self, message):
        self.wfile.write(message)
        self.wfile.flush()

    def _send_response(self, response_code: int, content_type: str, message):
        self.send_response(response_code)
        self.send_header('Content-type', content_type)
        self.end_headers()
        self.wfile.write(message)
        self.wfile.flush()

    def _send_error_message(self):
        self.send_response(404)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(bytes("Error occurred. Check the log for details.", "utf-8"))
        self.wfile.flush()

    def log_message(self, format, *args):
        log_info(format % args)
