import asyncore
import socket
import threading
import msgpack
import struct
from ..util.util import log_info, log_error, ClientQueue, ServerQueue
from ..util.util import interface


@interface
class MessageListener:

    def arrived(self, message: str=None):
        pass


class NetworkThread(threading.Thread):

    @property
    def local_port(self):
        return self._network_module.socket.getsockname()[1]

    @property
    def ip(self):
        return self._network_module.socket.getsockname()[0]

    @property
    def network(self):
        return self._network_module

    def __init__(self, network_module):
        threading.Thread.__init__(self, name='Rhino_network_thread')
        self._network_module = network_module

    def run(self):
        self._network_module.serve_forever()

    def shutdown(self):
        self._network_module.stop()


class NetworkServer(asyncore.dispatcher):

    def __init__(self, server_queue: ServerQueue, address: tuple, listener: MessageListener):
        asyncore.dispatcher.__init__(self)
        self._server_queue = server_queue
        self.__address = address
        self._message_listeners = []
        self._message_listeners.append(listener)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self._agent_name_ip_connector = {}
        self.terminate = False
        self.set_reuse_addr()
        self.server_bind()
        self.server_activate()

    def add_message_listener(self, message_listener):
        self._message_listeners.append(message_listener)

    def listeners(self):
        return self._message_listeners

    def server_bind(self):
        self.bind(self.__address)

    def register_agent(self, agent_name: str, ip_address: str, port_number: int):
        self._agent_name_ip_connector[(ip_address, port_number)] = agent_name

    def get_agent_name(self, client_address: tuple) -> str:
        return self._agent_name_ip_connector[client_address]

    def get_registered_agents(self) -> dict:
        return self._agent_name_ip_connector

    def server_activate(self):
        self.listen(50)

    def get_next_message(self, agent_name: str):
        return self._server_queue.get_next_message(agent_name)

    def message_number_for_agent(self, agent_name):
        return self._server_queue.messages_number(agent_name)

    def handle_accept(self):
        (conn_sock, client_address) = self.accept()
        ServerHandler(conn_sock, client_address, self)

    def stop(self):
        self.terminate = True

    def serve_forever(self):
        while not self.terminate:
            try:
                asyncore.loop(timeout=1, count=1)
            except Exception as exception:
                log_error(str(exception))


class ServerHandler(asyncore.dispatcher):
    """
    ServerHandler class is responsible for handling the connections
     with the connected clients
    """
    def __init__(self, conn_sock, client_address, server: NetworkServer):
        asyncore.dispatcher.__init__(self, conn_sock)
        self._network_server = server
        self._client_address = client_address
        self.is_writable = False

    def readable(self):
        return True

    def writable(self):
        _registered_clients = self._network_server.get_registered_agents()
        if self._client_address not in _registered_clients.keys():
            log_error('Client not found:{address}'.format(address=self._client_address))
            return
        _agent_name = _registered_clients[self._client_address]
        if self._network_server.message_number_for_agent(_agent_name) > 0:
            return True
        else:
            return False

    def handle_read(self):
        try:
            data_to_read = self.recv(4)
            if data_to_read:
                data_to_read_int = int(struct.unpack('>I', data_to_read)[0])
                data = self.recv(data_to_read_int)
                unpacked_data = msgpack.unpackb(data, encoding='utf-8')
                log_info('Server received data:{}'.format(unpacked_data))
                for listener in self._network_server.listeners():
                    listener.arrived(message=unpacked_data)
        except socket.error as error:
            log_error(str(error))

    def handle_write(self):
        _agent_name = self._network_server.get_agent_name(self._client_address)
        if _agent_name is None:
            log_error('Agent name is None')
            return
        message = self._network_server.get_next_message(_agent_name)
        log_info('[Network]: Sending message: %s ' % message)
        packed_msg = msgpack.packb(message, encoding='utf-8')
        msg_len = struct.pack('>I', len(packed_msg))
        self.send(msg_len + packed_msg)

    def handle_close(self):
        self.close()


class NetworkClient(asyncore.dispatcher):

    def send_message(self, message: str) -> None:
        self._client_queue.add_message(message)

    def __init__(self, client_queue: ClientQueue, address):
        asyncore.dispatcher.__init__(self)
        self._client_queue = client_queue
        self._message_listener = None
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        log_info("Client connecting to {}".format(address))
        self.connect(address)

    def set_message_listener(self, listener: MessageListener) -> None:
        self._message_listener = listener

    def handle_connect(self):
        pass

    def handle_close(self):
        self.close()

    def writable(self):
        if self._client_queue.messages_number() > 0:
            return True
        return False

    def handle_write(self):
        msg = self._client_queue.get_next_message()
        message = msgpack.packb(msg)
        msg_len = struct.pack('>I', len(message))
        self.send(msg_len+message)
        log_info("Message has been sent{}".format(msg))

    def handle_read(self):
        try:
            data_to_read = self.recv(4)
            if data_to_read:
                data_to_read_int = int(struct.unpack('>I', data_to_read)[0])
                data = self.recv(data_to_read_int)
                unpacked_data = msgpack.unpackb(data, encoding='utf-8')
                self._message_listener.arrived(message=unpacked_data)
                log_info('Client received data:{}'.format(unpacked_data))
        except socket.error as exception:
            log_error(str(exception))

    def stop(self):
        pass

    def serve_forever(self):
        asyncore.loop(timeout=1)
