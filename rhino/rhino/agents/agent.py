"""
ClientAgent and ServerAgent is responsible for starting the
network layer, initialize the configuration and
keep the connection with the Controller module
"""

from collections import defaultdict

from ..network.network import NetworkThread, MessageListener
from ..network.network import NetworkServer
from ..util.util import my_import, implements, log_info, log_error, get_registered_clients_from_config, ServerQueue
from ..pool.threadpool import ThreadPool
from ..msg.msgfactory import MsgFactory, Message, GetAvailableClientsMessage, SetTestConfigMessage, StatisticsMessage,\
    ClientInfoMessage, StartedMessage, StoppedMessage, LoginMessage, StopMessage, LogoutMessage, ErrorMessage
from ..network.webserver import RhinoHttpServer


@implements(MessageListener)
class ClientAgent:
    def __init__(self, agent_name: str,  network_thread: NetworkThread, thread_pool: ThreadPool):
        self._agent_name = agent_name
        self._network_thread = network_thread
        # configuration related variables
        self._registered_clients = []
        self._thread_pool = thread_pool
        # instance of the client which will perform the tests
        self._test_client = None
        self._test_client_name = None
        # if test configuration is set, then it is initialized
        self._initialized = 0  # 0: stopped, 1: started

    def send_message(self, message: str):
        self._client_queue.add_message(message)

    def initialize(self):
        self._network_thread.network.set_message_listener(self)
        # login into controller
        login_msg = LoginMessage(self._agent_name, self._network_thread.ip, self._network_thread.local_port)
        self._network_thread.network.send_message(login_msg.toJson())
        # get registered clients from config
        self._registered_clients = get_registered_clients_from_config()

    def start(self):
        self._network_thread.start()
        self._network_thread.network.send_message(StartedMessage(self._agent_name).toJson())

    def stop(self):
        self._thread_pool.stop_executors()
        self._network_thread.network.send_message(StoppedMessage(self._agent_name).toJson())

    def shutdown(self):
        self.stop()
        self._network_thread.network.send_message(LogoutMessage(self._agent_name).toJson())

    def arrived(self, message: str=None):
        log_info('[ClientAgent] Client listener: Message arrived: %s' % message)
        msg = MsgFactory.msg(message)
        if msg.type == Message.GET_AVAILABLE_CLIENTS:
            response = self.get_available_client_names()
            self._network_thread.network.send_message(response)

        elif msg.type == Message.ERROR:
            log_error(msg.error_message)
            print(msg.error_message)
            self.shutdown()

        elif msg.type == Message.START:
            if self._thread_pool is not None and self._test_client_name is not None:
                self._thread_pool.start_executors()
                msg = StartedMessage(self._agent_name)
                self._network_thread.network.send_message(msg.toJson())

        elif msg.type == Message.STOP:
            if self._thread_pool is not None and self._test_client_name is not None:
                self._thread_pool.stop_executors()
                msg = StoppedMessage(self._agent_name)
                self._network_thread.network.send_message(msg.toJson())

        elif msg.type == Message.SET_TEST_CONFIGURATION:
            self._set_test_configuration(msg)

        elif msg.type == Message.STATISTICS:
            if self._test_client is not None and self._test_client_name is not None:
                _client_statistics = self._test_client.statistics()
                msg = StatisticsMessage(self._agent_name, _client_statistics)
                self._network_thread.network.send_message(msg.toJson())
            else:
                log_error('[ClientAgent] Test client is None, cannot get statistics')

        elif msg.type == Message.CLIENT_INFO:
            msg = ClientInfoMessage(self._agent_name,
                                    self._initialized,
                                    self._test_client_name,
                                    self._thread_pool.thread_number,
                                    self._thread_pool.is_running)
            self._network_thread.network.send_message(msg.toJson())

    def _set_test_configuration(self, message: SetTestConfigMessage):
        if message is None:
            log_error('[ClientAgent] Configuration is None, cannot be set.')
            return
        self._test_client_name = message.client_name
        self._test_client = my_import(self._test_client_name)()
        self._thread_pool.initialise(self._test_client_name, message, message.simulate_client_number)
        log_info('[ClientAgent] Client initialized, threadPool has been created!!')
        self._initialized = 1

    def get_available_client_names(self) -> str:
        msg = GetAvailableClientsMessage(self._agent_name, self._registered_clients)
        return msg.toJson()


class ServerAgent:
    def __init__(self,
                 message_listener: MessageListener,
                 server_queue: ServerQueue,
                 lg_server: NetworkServer,
                 network_module: NetworkThread
                 ):
        # stores tuples (agent name, IP address)
        self._connected_clients = []
        self._message_listener = message_listener
        self._server_queue = server_queue
        self._networkModule = network_module
        self._lg_server = lg_server
        # store group name and the list of agent names
        self._groups = defaultdict(list)
        self._http_server = None

    def initialize(self):
        pass

    def start_web_server(self, configuration: dict) -> None:
        if configuration['web_server'] == 'True':
            self._http_server = RhinoHttpServer(configuration['web_server_listen_port'],configuration['web_directory'])
            self._http_server.set_server_agent(self)
            self._http_server.start()

    def create_group(self, group_name: str):
        if group_name not in self._groups.keys():
            self._groups[group_name] = list()

    def group_exists(self, group_name):
        if group_name is None:
            return False
        if group_name in self._groups:
            return True
        return False

    def delete_group(self, group_name: str):
        if group_name in self._groups:
            del self._groups[group_name]

    def get_groups(self):
        return ','.join([key for key in self._groups.keys()])

    def add_agents_to_group(self, group_name, agent_names):
        clients = agent_names.split(',')
        self._groups[group_name].extend(clients)

    def delete_agent_from_group(self, group_name: str, agent_name: str):
        if group_name not in self._groups:
            return
        self._groups[group_name].remove(agent_name)

    def get_group_members(self, group_name):
        return self._groups[group_name]

    def list_group_members(self, group_name):
        if group_name not in self._groups:
            return 'No group with name %s'.format(group_name)
        return str(self._groups[group_name])

    def register_agent(self, agent_name: str, ip_address: str, port_number: str):
        self._lg_server.register_agent(agent_name, ip_address, port_number)

    def get_next_message(self, agent_name: str):
        return self._server_queue.get_next_message(agent_name)

    def agent_with_name_exists(self, agent_name: str) ->bool:
        for agent in self._connected_clients:
            if agent[0] == agent_name:
                return True
        return False

    def remove_logout_agent(self, agent_name: str):
        for agent in self._connected_clients:
            if agent[0] == agent_name:
                self._connected_clients.remove(agent)

    def add_connected_agent(self, agent_data: tuple):
            self._connected_clients.append(agent_data)

    def get_connected_clients(self):
        return self._connected_clients

    def start(self):
        self._networkModule.start()

    def register_message_listener(self, message_listener):
        self._networkModule.network.add_message_listener(message_listener)

    def stop(self):
        if self._networkModule:
            self._networkModule.shutdown()
        self._lg_server.stop()

    def send(self, message: str, to_agent: str):
        self._server_queue.add_message(to_agent, message)

    def broadcast_message_to_agents(self, message: str):
        for _client in self._connected_clients:
            self.send(message=message, to_agent=_client[0])

    def send_stop_to_all_agents(self):
        stop = StopMessage()
        for _client in self._connected_clients:
            self.send(message=stop.toJson(), to_agent=_client[0])

    def shutdown(self):
        log_info('[ServerAgent] Server agent shutdown...')
        self.stop()
        if self._http_server:
            self._http_server.shutdown()
        log_info('[ServerAgent] Server agent shutdown...DONE')



