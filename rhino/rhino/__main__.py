import sys
from argparse import ArgumentParser
import logging
import socket
from .agents.agent import ClientAgent, ServerAgent
from .util.util import load_configuration_from_file
from .network.network import ServerQueue, NetworkServer, NetworkThread, ClientQueue, NetworkClient
from .pool.threadpool import ThreadPool

from .commandline import CommandLine

CONFIGURATION_FILE_NAME = 'rhino.config'


def generate_configuration_file():
    config_str = """
        {
            "log_level" : 2,
            "log_file": "rhino.log",
            "web_server": "False",
            "web_server_listen_port":8080,
            "web_directory":"www",
            "clients": [
                "rhino.clients.exampleclient.ExampleClient",
                "rhino.clients.memorycpuclient.MemoryCPUMeasureClient",
                "rhino.clients.sshclient.SSHClient"
            ]
        }
        """
    print(config_str)


def start_client_mode() -> None:
    _client_queue = ClientQueue()
    _lg_client = NetworkClient(_client_queue, (args.controller_ip, int(args.controller_port)))
    _network_thread = NetworkThread(_lg_client)
    _thread_pool = ThreadPool()
    client = ClientAgent(args.agent_name, _network_thread, _thread_pool)
    client.initialize()
    client.start()


def start_server_mode(configuration: dict) -> None:
    _cmd_line = CommandLine()
    _server_queue = ServerQueue()
    _lg_network_server = NetworkServer(_server_queue, (args.controller_ip, int(args.local_port)), _cmd_line)
    _networkModule = NetworkThread(_lg_network_server)

    server = ServerAgent(_cmd_line,
                         _server_queue,
                         _lg_network_server,
                         _networkModule)
    _cmd_line.set_server_agent(server)
    server.initialize()
    if configuration['web_server'] == 'True':
        server.start_web_server(configuration)
        server.start()
    _cmd_line.prompt = '(Lg) > '
    _cmd_line.cmdloop()


if __name__ == "__main__":
    parser = ArgumentParser()

    parser.add_argument('-init', '--initialize', dest='init', help='Command file to run', action='store_true')

    parser.add_argument('-c', '--config', dest='config', help='Configuration file')

    parser.add_argument('-m', '--mode', dest='mode', choices=['server', 'client'],
                        help='Start the tool in server or client mode')

    parser.add_argument('-f', '--file', dest='command_file', help='Command file to run')

    parser.add_argument('-n', '--name', dest='agent_name',
                        help='Name of the agent. This is required when the tool is started in client mode')

    parser.add_argument('-lp', '--local_port', dest='local_port', help='Local port to use')

    parser.add_argument('-ci', '--controller_ip', dest='controller_ip',
                        help='IP address of the controller (default: %(default)s)',
                        default='0.0.0.0')

    parser.add_argument('-cp', '--controller_port', dest='controller_port',
                        help='Port number used by the controller to listen')

    args = parser.parse_args()
    configuration = None
    if args.init:
        generate_configuration_file()
        sys.exit(0)
    if not args.config:
        print('None configuration file in parameters...Type help.')
        sys.exit(-1)
    else:
        success, configuration = load_configuration_from_file(configuration_file=args.config)
        if not success:
            print('Problem reading configuration file.')
            sys.exit(-1)
    if args.mode == 'client':
        logging.info(
            "Running in client mode, server address %s, server port: %s" % (args.controller_ip, args.controller_port))
        start_client_mode()
    elif args.mode == 'server':
        logging.info("Running in server mode, ip:{} port:{}".format(socket.gethostbyname(socket.gethostname()),
                                                                    args.local_port))
        start_server_mode(configuration)



