import cmd
import time
import glob
import os.path
import sys
from .agents.agent import ServerAgent
from .util.util import implements, log_debug, log_error
from .network.network import MessageListener
from .statistics import Statistics
from .msg.msgfactory import *

VERSION = '0.0.6'
AUTHOR = 'Ferenc Tollas'

@implements(MessageListener)
class CommandLine(cmd.Cmd):
    """
    {
        "command": "login"
        "agent_name": "name"
        "ip" : "127.0.0.1"
        "port" : 35633
    }
    {
        "command": "logout"
        "agent_name": "name"
    }

    {
        "command": "start"
    }

    {
        "command": "started"
    }

    {
        "command": "stop"
    }

    {
        "command": "stopped"
    }

    {
        "command": "get_available_clients"
    }

    {
        "command": "client_info",
    }
    {
        "command": "client_info",
        "agent_name": "agent_name",
        "initialized": 0|1
        "client_name": "TestClient"
        "threadpoolsize": 4,
        "state": 0|1
    }
    {
        "command": "set_test_config",
        "simulate_client_number": 4,
        "client_name": "com.test.TestClient",
        "test_type": "INFINITE",
        "test_methods": [
            {
                "method_name": "name",
                "method_execution_number": 0,
                "method_params": [
                    "param1",
                    "param2"
                ]
            }
        ]
    }

    {
        "command": "error",
        "agent_name": "agent name",
        "error_message": "Error happened"
    }

    {
        "command": "message",
        "agent_name": "agent name",
        "message": "Sample message"
    }

    {
        "command": "get_statistics",
    }

    {
        "command": "statistics",
        "agent_name": "agent name",
        "statistics": {}
    }
    """

    def __init__(self):
        super().__init__(completekey='Tab', stdin=sys.stdin, stdout=sys.stdout)
        self._server_agent = None
        self._statistics_thread = None

    def append_slash_if_dir(self, p):
        if p and os.path.isdir(p) and p[-1] != os.sep:
            return p + os.sep
        else:
            return p

    def path_completion(self, text, line, begidx, endidx):
        before_arg = line.rfind(" ", 0, begidx)
        if before_arg == -1:
            return  # arg not found

        fixed = line[before_arg+1:begidx]  # fixed portion of the arg
        arg = line[before_arg+1:endidx]
        pattern = arg + '*'
        completions = []
        for path in glob.glob(pattern):
            path = self.append_slash_if_dir(path)
            completions.append(path.replace(fixed, "", 1))
        return completions

    def do_load_file(self, line):
        if line is None or len(line) == 0:
            print('Missing file name. Type help load_file for details')
            return
        if not os.path.isfile(line):
            print('File {} does not exists'.format(line))
            return
        with open(line) as f:
            for _line in f:
                self.onecmd(_line)

    def complete_load_file(self, text, line, begidx, endidx):
        return self.path_completion(text, line, begidx, endidx)

    def help_load_file(self) -> None:
        print('Load commands from file. Usage:load_file <file_name>')

    def set_server_agent(self, server_agent: ServerAgent) -> None:
        self._server_agent = server_agent

    def do_start_statistics(self, line):
        if line is None or line == '':
            print('Missing parameter')
            return
        try:
            if self._statistics_thread is None:
                self._statistics_thread = Statistics(self._server_agent, sleep_seconds=3)
            self._statistics_thread.set_interval(int(line.strip()))
            self._statistics_thread.start()
        except Exception as e:
            print('Error. Time interval is wrong', str(e))

    def help_start_statistics(self):
        print('Starts statistics collecting from the agent:\n start_statistics <time interval in sec>')

    def do_stop_statistics(self, line):
        if self._statistics_thread.isAlive():
            self._statistics_thread.terminate()

    def help_stop_statistics(self, line):
        print('Stop collecting statistics')

    def do_version(self, line):
        print('Rhino version: {version}\nAuthor: {author} 2014'.format(**{'version': VERSION, 'author': AUTHOR}))

    def do_start(self, param):
        if param is None or param == '':
            print('Agent name is missing')
            return
        # check for group name
        if self._server_agent.group_exists(param):
                agent_names = self._server_agent.get_group_members(param)
                cmd = StartMessage()
                for agent in agent_names:
                    self._server_agent.send(message=cmd.toJson(), to_agent=agent)
                return
        # check for client
        if not self._has_client(param):
            print('Agent with name \'{name}\' is not connected'.format(name=param))
            return
        cmd = StartMessage()
        self._server_agent.send(message=cmd.toJson(), to_agent=param)

    def help_start(self):
        print("Starts the clients on and agent: \nstart <agent_name>")

    def help_stop(self):
        print("Stops the clients on an agent\n stop <agent_name>")

    def _has_client(self, agent_name: str) -> bool:
        _is_client = False
        for agents in self._server_agent.get_connected_clients():
            print(agents)
            if agent_name in agents:
                _is_client = True
                break
        return _is_client

    def do_stop(self, param):
        if param is None or param == '':
            print('Agent name is missing')
            return
        # check for group name
        if self._server_agent.group_exists(param):
                agent_names = self._server_agent.get_group_members(param)
                cmd = StopMessage()
                for agent in agent_names:
                    self._server_agent.send(message=cmd.toJson(), to_agent=agent)
                return

        if not self._has_client(param):
            print('Agent with name \'{name}\' is not connected'.format(name=param))
            return
        cmd = StopMessage()
        self._server_agent.send(message=cmd.toJson(), to_agent=param)

    def do_exit(self, line):
        if self._statistics_thread is not None and  self._statistics_thread.isAlive():
            self._statistics_thread.terminate()
        # send stop messages to all agents
        self._server_agent.send_stop_to_all_agents()
        self._server_agent.stop()
        self._server_agent.shutdown()
        sys.exit(0)

    def do_get_available_clients(self, agent_name=None):
        if agent_name is None or agent_name == '':
            print("Missing agent name")
            return
        msg = GetAvailableClientsMessage()
        self._server_agent.send(message=msg.toJson(), to_agent=agent_name)

    def help_get_available_clients(self):
        print("Gets the list of the available clients and methods on a given agent:\n"
              "get_available_clients <agent_name>")

    def do_set_test_config(self, param: str=None):
        if param is None or param == '':
            print("Missing parameters. Type 'help set_test_config'. ")
            return
        params = param.split(' ')

        if len(params) == 1 or params[0] is None or params[1] is None:
            print("Missing parameters. Type 'help set_test_config'. ")
            return
        # load the config file and send it to the client
        if not os.path.exists(params[1]):
            print('File does not exists:', params[1])
            return
        file = open(params[1], 'r')
        data = file.read()
        file.close()
        self._server_agent.send(message=data, to_agent=params[0])

    def complete_set_test_config(self, text, line, begidx, endidx):
        return self.path_completion(text, line, begidx, endidx)

    def do_set_group_test_config(self, param: str=None):
        if param is None or param == '':
            print("Missing parameters. Type 'help set_group_test_config'. ")
            return
        params = param.split(' ')

        if len(params) == 1 or params[0] is None or params[1] is None:
            print("Missing parameters. Type 'help set_test_config'. ")
            return
        # load the config file and send it to the client
        if not os.path.exists(params[1]):
            print('File does not exists:', params[1])
            return
        file = open(params[1], 'r')
        data = file.read()
        file.close()
        agent_names = self._server_agent.get_group_members(params[0])
        print(agent_names)
        for agent in agent_names:
            self._server_agent.send(message=data, to_agent=agent)

    def complete_set_group_test_config(self, text, line, begidx, endidx):
        return self.path_completion(text, line, begidx, endidx)

    def help_set_group_test_config(self, param: str=None):
        print("Sets the test configuration from file for a given group:\n"
              "set_group_test_config <group name> <config file name>")

    def help_set_test_config(self, param: str=None):
        print("Sets the test configuration from file for a given agent:\n"
              "set_test_config <agent_name> <config file name>")

    def do_get_connected_agents(self, line):
        # print(self._module.get_connected_clients())
        print('Connected agents')
        print('===================')
        _connected_clients = self._server_agent.get_connected_clients()
        for name in _connected_clients:
            print('{0:10} {1:3}'.format(name[0], name[1]))

    def help_get_connected_agents(self):
        print('Get the connected agent list')

    def do_create_group(self, line):
        self._server_agent.create_group(line)

    def help_create_group(self, line):
        print('Create agent group: create_group <group name>')

    def do_delete_group(self, line):
        self._server_agent.delete_group(line)

    def help_delete_group(self, line):
        print('Delete agent group: delete_group <group name>')

    def do_add_agents_to_group(self, line):
        # get the group
        if line is None or line == '':
            return
        group_name = line[0: line.index(' ')]
        agents = line[line.index(' ') + 1:]
        if len(agents.strip()) == 0:
            print('Type help for this command.')
            return
        self._server_agent.add_agents_to_group(group_name, agents)

    def help_add_agents_to_group(self):
        print('Add agents to group. add_agents_to_group <group name> <agents separated with comma>')

    def do_list_group_members(self, line):
        print(self._server_agent.list_group_members(line))

    def help_list_group_members(self):
        print('List group members')

    def do_list_groups(self, line):
        print(self._server_agent.get_groups())

    def help_list_groups(self, line):
        print('Lists available groups')

    def do_get_client_info(self, line):
        if line == '':
            print("Client name is empty")
            return
        cmd = ClientInfoMessage()
        self._server_agent.send(cmd.toJson(), to_agent=line)

    def help_get_client_info(self):
        print('Returns the methods and the parameters of the client')

    def emptyline(self):
        pass

    def arrived(self, message: str=None):
        """
        parse message and do the appropriate action
        :param message:
        :return:
        """

        log_debug('Received message:' + message)
        msg = MsgFactory.msg(message)

        if msg.type == Message.LOGIN:
            agent_name = msg.agent_name
            if self._server_agent.agent_with_name_exists(msg.agent_name):
                agent_name = msg.agent_name + '_' + str(int(round(time.time() * 1000)))
            self._server_agent.add_connected_agent((agent_name, msg.ip))
            self._server_agent.register_agent(agent_name, msg.ip, msg.port)

        elif msg.type == Message.LOGOUT:
            self._server_agent.remove_logout_agent(msg.agent_name)

        elif msg.type == Message.GET_AVAILABLE_CLIENTS:
            print('Available clients on agent ', msg.agent_name)
            print('================================================')
            for _client in msg.available_clients:
                print(_client)

        elif msg.type == Message.CLIENT_INFO:
            print(msg)
            print('Agent information ', msg.agent_name)
            print('================================================')
            print('Initialized:', 'True' if msg.initialized == 1 else 'False')
            print('Client name:', msg.client_name)
            print('Thread pool size:', msg.thread_pool_size)
            print('State:', 'Running' if msg.state == 1 else 'Not running')

        elif msg.type == Message.STATISTICS:
            with open('results.stat', 'a') as result_file:
                _d = msg.statistics
                s = ', '.join('{key}: {value}'.format(key=key, value=value) for key, value in _d.items())
                result_file.write(msg.agent_name + "," + s + '\n')
