from ..util.util import test_method, implements, Context
from ..clients.baseclient import BaseClient
import paramiko


@implements(BaseClient)
class SSHClient:

    def __init__(self):
        pass

    @test_method
    def login(self, host: str, port: int, username: str, password: str):
        self.client = paramiko.SSHClient()
        self.client.set_missing_host_key_policy(paramiko.WarningPolicy())
        print("Logging in")
        self.client.connect(host, port, username, password)

    @test_method
    def send_command(self, command: str):
        if self.client:
            _, stdout, stderr = self.client.exec_command(command)
            print("Stdout: %s" % stdout.read())
            print("Stderr: %s" % stderr.read())

    @test_method
    def logout(self):
        if self.client:
            self.client.close()
            print("Logged out")

    def initialise(self, parameters: dict, context: Context):
        pass

    def uninitialise(self):
        if self.client:
            self.client.close()

    def statistics(self) -> dict:
        return {'param': 1}
