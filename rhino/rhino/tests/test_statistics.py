from unittest import TestCase
from ..statistics import Statistics
from ..agents.agent import ServerAgent
from ..msg.msgfactory import StatisticsMessage


class MyServerAgent(ServerAgent):
    @property
    def message(self):
        return self._message

    def __init__(self):
        super().__init__(None, None, None, None)
        self._server_agent = None
        self._terminate = False
        self._sleep_seconds = 3
        self._message = ''

    def broadcast_message_to_agents(self, message):
        self._message = message


class MyStatistics(Statistics):
    def __init__(self, server_agent, sleep_sec):
        super().__init__(server_agent, sleep_sec)


class StatTests(TestCase):

    def test_init(self):
        statistics = MyStatistics(server_agent=None,sleep_sec=3)
        self.assertEqual(statistics._server_agent, None)
        self.assertEqual(statistics._terminate, False)
        self.assertEqual(statistics._sleep_seconds, 3)

    def test_set_interval(self):
        statistics = MyStatistics(server_agent=None, sleep_sec=3)
        statistics.set_interval(10)
        self.assertEqual(statistics._server_agent, None)
        self.assertEqual(statistics._terminate, False)
        self.assertEqual(statistics._sleep_seconds, 10)

    def test_terminate(self):
        statistics = MyStatistics(server_agent=None, sleep_sec=3)
        statistics.set_interval(10)
        self.assertEqual(statistics._server_agent, None)
        self.assertEqual(statistics._terminate, False)
        self.assertEqual(statistics._sleep_seconds, 10)

    def test_run(self):
        server_agent = MyServerAgent()
        statistics = MyStatistics(server_agent=server_agent, sleep_sec=3)
        statistics.set_interval(10)
        statistics._broadcast_message(StatisticsMessage())
        self.assertEqual(statistics._terminate, False)
        self.assertEqual(statistics._sleep_seconds, 10)
        self.assertEqual(server_agent.message, StatisticsMessage().toJson())


