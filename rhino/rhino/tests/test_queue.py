from ..util.util import ServerQueue, ClientQueue
from unittest import TestCase


class QueueTests(TestCase):

    def test_client_queue_no_message(self):
        queue = ClientQueue()
        self.assertEquals(0, queue.messages_number())

    def test_client_queue_add_None_message(self):
        queue = ClientQueue()
        queue.add_message(None)
        self.assertEquals(0, queue.messages_number())

    def test_client_queue_add_message(self):
        queue = ClientQueue()
        queue.add_message("test")
        self.assertEquals(1, queue.messages_number())

    def test_client_queue_remove_message(self):
        queue = ClientQueue()
        queue.add_message("test")
        self.assertEquals(1, queue.messages_number())
        msg = queue.get_next_message()
        self.assertEquals('test', msg)

    def test_client_queue_clear_messages(self):
        queue = ClientQueue()
        queue.add_message("test")
        queue.add_message("test2")
        self.assertEquals(2, queue.messages_number())
        queue.clear_messages()
        self.assertEquals(0, queue.messages_number())

    def test_server_queue_add_message(self):
        server_queue = ServerQueue()
        server_queue.add_message('agent1', 'test')
        server_queue.add_message('agent2', 'test2')
        self.assertEquals(1, server_queue.messages_number('agent1'))

    def test_server_queue_add_None_message(self):
        server_queue = ServerQueue()
        server_queue.add_message('agent1', None)
        server_queue.add_message('agent2', 'test2')
        self.assertEquals(0, server_queue.messages_number('agent1'))

    def test_server_queue_add_message_to_None_agent(self):
        server_queue = ServerQueue()
        server_queue.add_message(None, 'test1')
        server_queue.add_message('agent2', 'test2')
        self.assertEquals(0, server_queue.messages_number('agent1'))

    def test_server_queue_clear_message(self):
        server_queue = ServerQueue()
        server_queue.add_message('agent1', 'test')
        server_queue.add_message('agent2', 'test2')
        server_queue.clear_messages('agent1')
        self.assertEquals(0, server_queue.messages_number('agent1'))
        self.assertEquals(1, server_queue.messages_number('agent2'))

    def test_server_queue_get_next_message(self):
        server_queue = ServerQueue()
        server_queue.add_message('agent1', 'test')
        server_queue.add_message('agent1', 'test1')
        server_queue.add_message('agent2', 'test2')
        self.assertEquals('test', server_queue.get_next_message('agent1'))
        self.assertEquals('test2', server_queue.get_next_message('agent2'))

