from unittest import TestCase
from ..agents.agent import ServerAgent
from ..util.util import implements
from ..network.network import MessageListener, ServerQueue


class MyServerQueue:
    def __init__(self):
        pass

@implements(MessageListener)
class MyMessageListener():
    def __init__(self):
        self._arrived = False

    def arrived(self, message: str=None):
        self._arrived = True


class AgentTest(TestCase):

    def test_create_group(self):
        server_agent = ServerAgent(None, None, None, None)
        server_agent.initialize()
        server_agent.create_group('test_group')
        _groups = server_agent.get_groups()
        self.assertTrue('test_group' in _groups)

    def test_delete_group(self):
        server_agent = ServerAgent(None, None, None, None)
        server_agent.initialize()
        server_agent.create_group('test_group')
        _groups = server_agent.get_groups()
        self.assertTrue('test_group' in _groups)
        server_agent.delete_group('test_group')
        _groups = server_agent.get_groups()
        self.assertTrue('test_group' not in _groups)

    def test_create_group_add_agent(self):
        server_agent = ServerAgent(None, None, None, None)
        server_agent.initialize()
        server_agent.create_group('test_group')
        server_agent.add_agents_to_group('test_group', 'agent1')
        members = server_agent.get_group_members('test_group')
        self.assertTrue('agent1' in members)


    def test_create_group_remove_agent(self):
        server_agent = ServerAgent(None, None, None, None)
        server_agent.initialize()
        server_agent.create_group('test_group')
        server_agent.add_agents_to_group('test_group', 'agent1')
        members = server_agent.get_group_members('test_group')
        self.assertTrue('agent1' in members)
        server_agent.delete_agent_from_group('test_group', 'agent1')
        members = server_agent.get_group_members('test_group')
        self.assertTrue('agent1' not in members)

    def test_send_message(self):
        server_queue = ServerQueue()
        server_agent = ServerAgent(MyMessageListener(), server_queue, None, None)
        server_agent.send('test', 'test_agent')
        self.assertEqual('test', server_agent.get_next_message('test_agent'))
