from unittest import TestCase
from ..msg.msgfactory import *

import json


class MsgTest(TestCase):
    def setUp(self):
        pass

    def test_login_message(self):
        login_msg = LoginMessage('name', '127.0.0.1', '123')
        self.assertEquals('name', login_msg.agent_name)
        self.assertEquals('127.0.0.1', login_msg.ip)
        self.assertEquals('123', login_msg.port)
        m = json.loads(login_msg.toJson())
        self.assertEquals('name', m['agent_name'])
        self.assertEquals('127.0.0.1', m['ip'])
        self.assertEquals('123', m['port'])

    def test_logout_message(self):
        logout_msg = LogoutMessage('agent_name')
        self.assertEquals('agent_name', logout_msg.agent_name)
        m = json.loads(logout_msg.toJson())
        self.assertEquals('agent_name', m['agent_name'])

    def test_start_message(self):
        start_msg = StartMessage()
        m = json.loads(start_msg.toJson())
        self.assertEquals('start', m['command'])

    def test_started_message(self):
        start_msg = StartedMessage('name')
        self.assertEquals('name', start_msg.agent_name)
        m = json.loads(start_msg.toJson())
        self.assertEquals('started', m['command'])
        self.assertEquals('name', m['agent_name'])

    def test_stop_message(self):
        start_msg = StopMessage()
        m = json.loads(start_msg.toJson())
        self.assertEquals('stop', m['command'])

    def test_stopped_message(self):
        stopped_msg = StoppedMessage('name')
        self.assertEquals('name', stopped_msg.agent_name)
        m = json.loads(stopped_msg.toJson())
        self.assertEquals('stopped', m['command'])
        self.assertEquals('name', m['agent_name'])

    def test_get_available_clients(self):
        msg = GetAvailableClientsMessage('agent_name', ['a', 'b'])
        self.assertEquals('agent_name', msg.agent_name)
        self.assertEquals(['a', 'b'], msg.available_clients)
        m = json.loads(msg.toJson())
        self.assertEquals('agent_name', m['agent_name'])
        self.assertEquals('available_clients', m['command'])
        self.assertEquals(['a', 'b'], m['available_clients'])

    def test_client_info_message(self):
        msg = ClientInfoMessage('agent_name',
                                False,
                                'client_name',
                                4,
                                False)
        self.assertEquals('agent_name', msg.agent_name)
        self.assertEquals(False, msg.initialized)
        self.assertEquals('client_name', msg.client_name)
        self.assertEquals(4, msg.thread_pool_size)
        self.assertEquals(False, msg.state)
        m = json.loads(msg.toJson())
        self.assertEquals('client_info', m['command'])
        self.assertEquals('agent_name', m['agent_name'])
        self.assertEquals(0, m['initialized'])
        self.assertEquals(4, m['threadpoolsize'])
        self.assertEquals(0, m['state'])

    def test_error_message(self):
        msg = ErrorMessage('agent_name', 'error')
        self.assertEquals('agent_name', msg.agent_name)
        self.assertEquals('error', msg.error_message)
        m = json.loads(msg.toJson())
        self.assertEquals('error', m['command'])
        self.assertEquals('agent_name', m['agent_name'])
        self.assertEquals('error', m['error_message'])

    def test_info_message(self):
        msg = InfoMessage('agent_name', 'sample')
        self.assertEquals('agent_name', msg.agent_name)
        self.assertEquals('sample', msg.message)
        m = json.loads(msg.toJson())
        self.assertEquals('message', m['command'])
        self.assertEquals('agent_name', m['agent_name'])
        self.assertEquals('sample', m['message'])

    def test_statistics_message(self):
        msg = StatisticsMessage('agent_name', {'a': 3, 'b': 4})
        self.assertEquals('agent_name', msg.agent_name)
        self.assertEquals( {'a': 3, 'b': 4}, msg.statistics)
        m = json.loads(msg.toJson())
        self.assertEquals('statistics', m['command'])
        self.assertEquals('agent_name', m['agent_name'])
        self.assertEquals({'a': 3, 'b': 4}, m['statistics'])

    def test_GetConnectedAgentsMessage(self):
        data = [('a','127.0.0.1'), ('b', '127.0.0.2')]
        msg = GetConnectedAgentsMessage(data)
        print(msg.toJson())
        json_str ="""
            {"agents": [{"ip": "127.0.0.1", "name": "a"}, {"ip": "127.0.0.2", "name": "b"}], "command": "get_connected_agents"}
            """
        self.assertEqual(json_str.strip(), msg.toJson())

    def test_factory(self):
        login_msg = LoginMessage('name', '127.0.0.1', '123')
        logout_msg = LogoutMessage('agent_name')
        start_msg = StartMessage()
        started_msg = StartedMessage('name')
        stop_msg = StopMessage()
        stopped_msg = StoppedMessage('name')
        get_available_clients = GetAvailableClientsMessage('agent_name', ['a', 'b'])
        client_info_message = ClientInfoMessage('agent_name',
                                False,
                                'client_name',
                                4,
                                False)
        error_message = ErrorMessage('agent_name', 'error')
        info_message = InfoMessage('agent_name', 'sample')
        statistics_message = StatisticsMessage('agent_name', {'a': 3, 'b': 4})

        msg = MsgFactory.msg(login_msg.toJson())
        self.assertEquals(msg.type, Message.LOGIN)


        msg = MsgFactory.msg(logout_msg.toJson())
        self.assertEquals(msg.type, Message.LOGOUT)


        msg = MsgFactory.msg(start_msg.toJson())
        self.assertEquals(msg.type, Message.START)

        msg = MsgFactory.msg(started_msg.toJson())
        self.assertEquals(msg.type, Message.STARTED)


        msg = MsgFactory.msg(stop_msg.toJson())
        self.assertEquals(msg.type, Message.STOP)


        msg = MsgFactory.msg(stopped_msg.toJson())
        self.assertEquals(msg.type, Message.STOPPED)


        msg = MsgFactory.msg(get_available_clients.toJson())
        self.assertEquals(msg.type, Message.GET_AVAILABLE_CLIENTS)


        msg = MsgFactory.msg(error_message.toJson())
        self.assertEquals(msg.type, Message.ERROR)


        msg = MsgFactory.msg(info_message.toJson())
        self.assertEquals(msg.type, Message.INFO)

        msg = MsgFactory.msg(statistics_message.toJson())
        self.assertEquals(msg.type, Message.STATISTICS)

        msg = MsgFactory.msg(client_info_message.toJson())
        self.assertEquals(msg.type, Message.CLIENT_INFO)

