from unittest import TestCase
from ..commandline import CommandLine
import sys
from io import StringIO

class MyCommand(CommandLine):
    def __init__(self):
        super().__init__()


class MainTests(TestCase):

    def test_init(self):
        cmd = CommandLine()
        self.assertIsNone(cmd._server_agent)
        self.assertIsNone(cmd._statistics_thread)

    def test_help_start_statistics(self):
        old = sys.stdout
        output_win = StringIO()
        sys.stdout = output_win
        cmd = CommandLine()
        cmd.help_start_statistics()
        self.assertEqual('Starts statistics collecting from the agent:\n start_statistics <time interval in sec>\n', output_win.getvalue())
        sys.stdout = old

    def test_help_stop_statistics(self):
        old = sys.stdout
        output_win = StringIO()
        sys.stdout = output_win
        cmd = CommandLine()
        cmd.help_stop_statistics('')
        self.assertEqual('Stop collecting statistics\n', output_win.getvalue())
        sys.stdout = old

    def test_help_start(self):
        old = sys.stdout
        output_win = StringIO()
        sys.stdout = output_win
        cmd = CommandLine()
        cmd.help_start()
        self.assertEqual('Starts the clients on and agent: \nstart <agent_name>', output_win.getvalue().rstrip())
        sys.stdout = old

    def test_help_stop(self):
        old = sys.stdout
        output_win = StringIO()
        sys.stdout = output_win
        cmd = CommandLine()
        cmd.help_stop()
        self.assertEqual('Stops the clients on an agent\n stop <agent_name>', output_win.getvalue().rstrip())
        sys.stdout = old

    def test_help_get_available_clients(self):
        old = sys.stdout
        output_win = StringIO()
        sys.stdout = output_win
        cmd = CommandLine()
        cmd.help_get_available_clients()
        self.assertEqual("Gets the list of the available clients and methods on a given agent:\n"
                      "get_available_clients <agent_name>", output_win.getvalue().rstrip())
        sys.stdout = old

    def test_help_set_group_test_config(self):
        old = sys.stdout
        output_win = StringIO()
        sys.stdout = output_win
        cmd = CommandLine()
        cmd.help_set_group_test_config('')
        self.assertEqual("Sets the test configuration from file for a given group:\n"
                      "set_group_test_config <group name> <config file name>", output_win.getvalue().rstrip())
        sys.stdout = old

    def test_set_test_config(self):
        old = sys.stdout
        output_win = StringIO()
        sys.stdout = output_win
        cmd = CommandLine()
        cmd.help_set_test_config('')
        self.assertEqual("Sets the test configuration from file for a given agent:\n"
                      "set_test_config <agent_name> <config file name>", output_win.getvalue().rstrip())
        sys.stdout = old

    def test_help_get_connected_agents(self):
        old = sys.stdout
        output_win = StringIO()
        sys.stdout = output_win
        cmd = CommandLine()
        cmd.help_get_connected_agents()
        self.assertEqual('Get the connected agent list', output_win.getvalue().rstrip())
        sys.stdout = old

    def test_help_create_group(self):
        old = sys.stdout
        output_win = StringIO()
        sys.stdout = output_win
        cmd = CommandLine()
        cmd.help_create_group('')
        self.assertEqual('Create agent group: create_group <group name>', output_win.getvalue().rstrip())
        sys.stdout = old


    def test_help_delete_group(self):
        old = sys.stdout
        output_win = StringIO()
        sys.stdout = output_win
        cmd = CommandLine()
        cmd.help_delete_group('')
        self.assertEqual('Delete agent group: delete_group <group name>', output_win.getvalue().rstrip())
        sys.stdout = old

    def test_help_add_agents_to_group(self):
        old = sys.stdout
        output_win = StringIO()
        sys.stdout = output_win
        cmd = CommandLine()
        cmd.help_add_agents_to_group()
        self.assertEqual('Add agents to group. add_agents_to_group <group name> <agents separated with comma>', output_win.getvalue().rstrip())
        sys.stdout = old

    def test_help_list_group_members(self):
        old = sys.stdout
        output_win = StringIO()
        sys.stdout = output_win
        cmd = CommandLine()
        cmd.help_list_group_members()
        self.assertEqual('List group members',
                         output_win.getvalue().rstrip())
        sys.stdout = old


    def test_help_list_groups(self):
        old = sys.stdout
        output_win = StringIO()
        sys.stdout = output_win
        cmd = CommandLine()
        cmd.help_list_groups('')
        self.assertEqual('Lists available groups',
                         output_win.getvalue().rstrip())
        sys.stdout = old


    def test_help_get_client_info(self):
        old = sys.stdout
        output_win = StringIO()
        sys.stdout = output_win
        cmd = CommandLine()
        cmd.help_get_client_info()
        self.assertEqual('Returns the methods and the parameters of the client',
                         output_win.getvalue().rstrip())
        sys.stdout = old
