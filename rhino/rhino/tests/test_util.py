from unittest import TestCase
from ..util.util import ClientQueue, ServerQueue, Context


class MyClientQueue(ClientQueue):

    def __init__(self):
        super().__init__()


class MyServerQueue(ServerQueue):

    def __init__(self):
        super().__init__()

    def get_element_number(self):
        return len(self._queue)

    def get_messages_of_client(self, client_name: str):
        return self._queue[client_name]


class UtilTest(TestCase):
    def setUp(self):
        self._client_queue = MyClientQueue()
        self._server_queue = MyServerQueue()
        self._context = Context(self._client_queue)

    def tearDown(self):
        pass

    def test_client_queue_add_NONE_message(self):
        self._client_queue.add_message(None)
        self.assertTrue(0 == self._client_queue.messages_number())

    def test_client_queue_add_message(self):
        self._client_queue.add_message('Test msg')
        self.assertTrue(1 == self._client_queue.messages_number())

    def test_client_queue_clear_messages(self):
        self._client_queue.add_message('Test msg')
        self._client_queue.clear_messages()
        self.assertTrue(0 == self._client_queue.messages_number())

    def test_client_queue_pop_message(self):
        self._client_queue.add_message('Test msg')
        self._client_queue.add_message('Test msg2')
        msg = self._client_queue.get_next_message()
        self.assertTrue(msg == 'Test msg')
        self.assertTrue(1 == self._client_queue.messages_number())
        msg = self._client_queue.get_next_message()
        self.assertTrue(msg == 'Test msg2')
        self.assertTrue(0 == self._client_queue.messages_number())

    def test_server_queue_add_None_message(self):
        self._server_queue.add_message(None, None)
        self.assertTrue(0 == self._server_queue.get_element_number())

    def test_server_queue_add_message(self):
        self._server_queue.add_message('a', 'b')
        self.assertTrue(1 == self._server_queue.get_element_number())

    def test_server_queue_add_2_messages(self):
        self._server_queue.add_message('a', 'b')
        self._server_queue.add_message('a', 'c')
        msgs = self._server_queue.get_messages_of_client('a')
        self.assertTrue(2 == len(msgs))

    def test_server_queue_add_2_messages_1_pop(self):
        self._server_queue.add_message('a', 'b')
        self._server_queue.add_message('a', 'c')
        _msg = self._server_queue.get_next_message('a')
        self.assertTrue('b' == _msg)
        msgs = self._server_queue.get_messages_of_client('a')
        self.assertTrue(1 == len(msgs))

    def test_server_queue_clear(self):
        self._server_queue.add_message('a', 'b')
        self._server_queue.add_message('a', 'c')
        self._server_queue.clear_messages('a')
        msgs = self._server_queue.get_messages_of_client('a')
        self.assertTrue(0 == len(msgs))

    def test_server_get_unknown_client(self):
        self._server_queue.add_message('a', 'b')
        self._server_queue.add_message('a', 'c')
        self._server_queue.clear_messages('c')
        msgs = self._server_queue.get_messages_of_client('c')
        self.assertTrue(0 == len(msgs))

    def test_context(self):
        self._context.send_message('message1')
        self.assertTrue(1 == self._client_queue.messages_number())
