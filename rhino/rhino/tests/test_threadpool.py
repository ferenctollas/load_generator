from unittest import TestCase
from ..pool.threadpool import ThreadPool
from ..pool.threadpool import ExecutorThread
from ..util.util import Context, ClientQueue
from ..msg.msgfactory import SetTestConfigMessage


class TestClient:
    def initialise(self, param, context):
        pass

    def uninitialise(self):
        pass


class MyThreadPool(ThreadPool):
    def __init__(self, thread_number=1, client_queue: ClientQueue=None):
        super().__init__(thread_number=thread_number, client_queue=client_queue)


class MyExecutor(ExecutorThread):
    @property
    def called(self):
        return self._run_called

    @property
    def infinite_called(self):
        return self._run_infinite_called

    @property
    def time_limited_called(self):
        return self._run_time_limited_called

    def __init__(self, test_client=None, test_configuration: SetTestConfigMessage=None, context: Context=None):
        super().__init__(test_client=test_client, test_configuration=test_configuration, context=Context)
        self._run_called = False
        self._run_infinite_called = False
        self._run_time_limited_called = False

    def _run_test_once(self):
        self._run_called = True

    def _run_infinite_test(self):
        self._run_infinite_called = True

    def _run_time_limited_test(self):
        self._run_time_limited_called = True


class ThreadPoolTest(TestCase):

    def test_init(self):
        my_thread_pool = MyThreadPool(thread_number=2, client_queue=None)
        self.assertEqual(my_thread_pool.thread_number, 2)
        self.assertEqual(my_thread_pool.test_configuration, None)
        self.assertEqual(my_thread_pool.client_name, None)
        self.assertFalse(my_thread_pool.is_running)

    def test_initialize(self):
        my_thread_pool = MyThreadPool(thread_number=2, client_queue=None)
        self.assertEqual(my_thread_pool.thread_number, 2)
        self.assertEqual(my_thread_pool.test_configuration, None)
        self.assertEqual(my_thread_pool.client_name, None)
        self.assertFalse(my_thread_pool.is_running)

        config_msg = SetTestConfigMessage(1, 'client_name', 'SEQUENCE', 300, {}, "")
        my_thread_pool.initialise('com.test', config_msg, 300)
        self.assertEqual(my_thread_pool.thread_number, 300)
        self.assertEqual(my_thread_pool.test_configuration, config_msg)
        self.assertEqual(my_thread_pool.client_name, 'com.test')
        self.assertFalse(my_thread_pool.is_running)

    def test_executor_thread_sequence(self):
        config_msg = SetTestConfigMessage(1, 'client_name', 'SEQUENCE', 300, {}, "")

        # __init__(self, test_client=None, test_configuration: SetTestConfigMessage=None, context: Context=None):
        c = Context(None)
        my_executor = MyExecutor(test_client=TestClient(), test_configuration=config_msg, context=c)
        my_executor.run()
        self.assertTrue(my_executor.called)

    def test_executor_thread_infinite(self):
        config_msg = SetTestConfigMessage(1, 'client_name', 'INFINITE', 300, {}, "")

        # __init__(self, test_client=None, test_configuration: SetTestConfigMessage=None, context: Context=None):
        c = Context(None)
        my_executor = MyExecutor(test_client=TestClient(), test_configuration=config_msg, context=c)
        my_executor.run()
        self.assertTrue(my_executor.infinite_called)

    def test_executor_thread_time_limited(self):
        config_msg = SetTestConfigMessage(1, 'client_name', 'TIME_LIMITED', 300, {}, "")

        # __init__(self, test_client=None, test_configuration: SetTestConfigMessage=None, context: Context=None):
        c = Context(None)
        my_executor = MyExecutor(test_client=TestClient(), test_configuration=config_msg, context=c)
        my_executor.run()
        self.assertTrue(my_executor.time_limited_called)

    def test_context_add(self):
        context = Context(client_queue=None)
        context.register_object('test', 'test_value')
        value = context.get_object('test')
        self.assertEquals('test_value', value)

    def test_context_delete(self):
        context = Context(client_queue=None)
        context.register_object('test', 'test_value')
        value = context.get_object('test')
        self.assertEquals('test_value', value)
        context.unregister_object('test')
        value = context.get_object('test')
        self.assertEquals(None, value)
