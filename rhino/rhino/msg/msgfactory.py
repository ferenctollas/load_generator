import json


class Message:
    START = 0
    STOP = 1
    GET_AVAILABLE_CLIENTS = 2
    CLIENT_INFO = 3
    SET_TEST_CONFIGURATION = 4
    ERROR = 5
    INFO = 6
    STATISTICS = 7
    LOGIN = 8
    LOGOUT = 9
    STARTED = 10
    STOPPED = 11
    NONE = 12
    GET_CONNECTED_AGENTS = 13

    @property
    def type(self) -> int:
        return self._msg_type


    @property
    def agent_name(self) -> str:
        return self._agent_name

    @agent_name.setter
    def agent_name(self, name: str) -> None:
         self._agent_name = name


    @property
    def group_name(self) -> str:
        return self._group_name

    @group_name.setter
    def group_name(self, name: str) -> None:
        self._group_name = name

    def __init__(self, msg_type: int):
        self._msg_type = msg_type
        self._agent_name = None
        self._group_name = None

    def toJson(self) -> str:
        return ""


class LoginMessage(Message):
    """
     {
        "command": "login"
        "agent_name": "name"
        "ip" : "127.0.0.1"
        "port" : 35633
    }
    """

    @property
    def agent_name(self):
        return self._agent_name

    @property
    def ip(self):
        return self._ip

    @property
    def port(self):
        return self._port

    def __init__(self, agent_name: str, ip: str, port: str):
        super().__init__(Message.LOGIN)
        self._agent_name = agent_name
        self._ip = ip
        self._port = port

    def toJson(self):
        return json.dumps(
            {
                "command": "login",
                "agent_name": self._agent_name,
                "ip": self._ip,
                "port": self._port
            }
        )


class LogoutMessage(Message):
    """
    {
        "command": "logout"
        "agent_name": "name"
    }
    """

    @property
    def agent_name(self):
        return self._agent_name

    def __init__(self, agent_name: str):
        super().__init__(Message.LOGOUT)
        self._agent_name = agent_name

    def toJson(self):
        return json.dumps(
            {
                "command": "logout",
                "agent_name": self._agent_name
            }
        )


class StartMessage(Message):

    def __init__(self):
        super().__init__(Message.START)

    def toJson(self):
        return json.dumps({
            'command': 'start'
        })


class StartedMessage(Message):
    @property
    def agent_name(self):
        return self._agent_name

    def __init__(self, agent_name: str):
        super().__init__(Message.STARTED)
        self._agent_name = agent_name

    def toJson(self):
        return json.dumps({
            "command": "started",
            "agent_name": self._agent_name
        })


class StopMessage(Message):
    def __init__(self):
        super().__init__(Message.STOP)

    def toJson(self):
        return json.dumps(
            {
                "command": "stop"
            }
        )


class StoppedMessage(Message):
    @property
    def agent_name(self):
        return self._agent_name

    def __init__(self, agent_name: str):
        super().__init__(Message.STOPPED)
        self._agent_name = agent_name

    def toJson(self):
        return json.dumps(
            {
                "command": "stopped",
                "agent_name": self._agent_name
            }
        )


class GetAvailableClientsMessage(Message):
    """
        {"command": "available_clients",
         "agent_name": self._agent_name,
         "available_clients": self._registered_clients
        }
    """

    @property
    def agent_name(self):
        return self._agent_name

    @property
    def available_clients(self):
        return self._available_clients

    def __init__(self, agent_name: str=None, available_clients=[]):
        super().__init__(Message.GET_AVAILABLE_CLIENTS)
        self._agent_name = agent_name
        self._available_clients = available_clients

    def toJson(self):
        return json.dumps(
            {"command": "available_clients",
             "agent_name": self._agent_name,
             "available_clients": self._available_clients
            }
        )


class ClientInfoMessage(Message):
    """
    {
            "command": "client_info",
            "agent_name": "agent_name",
            "initialized": 0|1
            "client_name": "TestClient"
            "threadpoolsize": 4,
            "state": 0|1
        }
    """

    @property
    def agent_name(self):
        return self._agent_name

    @property
    def initialized(self):
        return self._initialized

    @property
    def client_name(self):
        return self._client_name

    @property
    def thread_pool_size(self):
        return self._thread_pool_size

    @property
    def state(self):
        return self._state

    def __init__(self, agent_name: str=None, initialized: bool=False, client_name: str=None, thread_pool_size: int=0,
                 state: bool=False):
        super().__init__(Message.CLIENT_INFO)
        self._agent_name = agent_name
        self._initialized = initialized
        self._client_name = client_name
        self._thread_pool_size = thread_pool_size
        self._state = state

    def toJson(self):
        return json.dumps(
            {
                "command": "client_info",
                "agent_name": self._agent_name,
                "initialized": 1 if self._initialized else 0,
                "client_name": self._client_name,
                "threadpoolsize": self._thread_pool_size,
                "state": 1 if self._state else 0
            }
        )


class SetTestConfigMessage(Message):
    """
    {
            "command": "set_test_config",
            "simulate_client_number": 4,
            "client_name": "com.test.TestClient",
            "test_type": "INFINITE",
            "test_methods": [
                {
                    "method_name": "name",
                    "method_execution_number": 0,
                    "method_params": [
                        "param1",
                        "param2"
                    ]
                }
            ]
        }
    """

    @property
    def simulate_client_number(self):
        return self._simulate_client_number

    @property
    def client_name(self):
        return self._client_name

    @property
    def test_type(self):
        return self._test_type

    @property
    def test_methods(self):
        return self._test_methods

    @property
    def test_time(self):
        return self._time

    @property
    def parameters(self) -> dict:
        return self._parameters

    def __init__(self, simulate_client_number: int, client_name: str, test_type: str, time: int, parameters: dict,
                 test_methods):
        super().__init__(Message.SET_TEST_CONFIGURATION)
        self._simulate_client_number = simulate_client_number
        self._client_name = client_name
        self._test_type = test_type
        self._time = time
        self._parameters = parameters
        self._test_methods = test_methods

    def toJson(self):
        return json.dumps(
            {
                "command": "set_test_config",
                "simulate_client_number": self._simulate_client_number,
                "client_name": self._client_name,
                "test_type": self._test_type,
                "test_methods": self._test_methods
            }
        )


class ErrorMessage(Message):
    """
     {
        "command": "error",
        "agent_name": "agent name",
        "error_message": "Error happened"
    }
    """

    @property
    def agent_name(self):
        return self._agent_name

    @property
    def error_message(self):
        return self._error_message

    def __init__(self, agent_name: str, error_message: str):
        super().__init__(Message.ERROR)
        self._agent_name = agent_name
        self._error_message = error_message

    def toJson(self):
        return json.dumps(
            {
                "command": "error",
                "agent_name": self._agent_name,
                "error_message": self._error_message
            }
        )


class InfoMessage(Message):
    """
    {
        "command": "message",
        "agent_name": "agent name",
        "message": "Sample message"
    }
    """

    @property
    def agent_name(self):
        return self._agent_name

    @property
    def message(self):
        return self._message

    def __init__(self, agent_name: str, message: str):
        super().__init__(Message.INFO)
        self._agent_name = agent_name
        self._message = message

    def toJson(self):
        return json.dumps(
            {
                "command": "message",
                "agent_name": self._agent_name,
                "message": self._message
            }
        )


class StatisticsMessage(Message):
    """
    {
        "command": "statistics",
        "agent_name": "agent name",
        "statistics": {}
    }
    """

    @property
    def agent_name(self):
        return self._agent_name

    @property
    def statistics(self):
        return self._statistics

    def __init__(self, agent_name: str=None, statistics: dict=None):
        super().__init__(Message.STATISTICS)
        self._agent_name = agent_name
        self._statistics = statistics

    def toJson(self):
        return json.dumps({
            "command": "statistics",
            "agent_name": self._agent_name,
            "statistics": self._statistics
        }
        )


class GetConnectedAgentsMessage(Message):
    """
{
        "command": "get_connected_agents"
        "agents": [
            {
                "name": "agent_name"
                "ip": "127.0.0.1"
                "port": 123
            }
        ]
}
    """

    @property
    def agents(self):
        return self._agents

    def __init__(self, agents: list):
        """
        :param agents: Contains list of tuples with agent name, IP and port
        :type agents:
        :return:
        :rtype:
        """
        super().__init__(Message.GET_CONNECTED_AGENTS)
        self._agents = agents

    def toJson(self):
        agents = []
        for agent in self._agents:
            agents.append(
                            {
                                'name':agent[0],
                                'ip': agent[1]
                            }
                        )
        return json.dumps(
            {
                "command": "get_connected_agents",
                "agents": agents
            })


class MsgFactory:
    @classmethod
    def msg(cls, message: str) -> Message:
        _msg = json.loads(message)

        if _msg['command'] == "start":
            msg = StartMessage()
            if 'agent_name' in _msg:
                msg.agent_name = _msg['agent_name']
            if 'group_name' in _msg:
                msg.group_name = _msg['group_name']

            return msg

        elif _msg['command'] == "stop":
            msg = StopMessage()
            if 'agent_name' in _msg:
                msg.agent_name = msg['agent_name']
            if 'group_name' in _msg:
                msg.group_name = msg['group_name']

            return msg

        elif _msg['command'] == "login":
            return LoginMessage(_msg['agent_name'], _msg['ip'], _msg['port'])

        elif _msg['command'] == "logout":
            return LogoutMessage(_msg['agent_name'])

        elif _msg['command'] == "available_clients":
            if _msg['agent_name'] and _msg['available_clients']:
                return GetAvailableClientsMessage(agent_name=_msg['agent_name'],
                                                  available_clients=_msg['available_clients'])
            else:
                return GetAvailableClientsMessage()
                # 17 1945
        elif _msg['command'] == "client_info":
            return ClientInfoMessage(_msg['agent_name'],
                                     _msg['initialized']if 'initialized' in _msg else '',
                                     _msg['client_name']if 'client_name' in _msg else '',
                                     _msg['threadpoolsize']if 'threadpoolsize' in _msg else '',
                                     _msg['state'] if 'state' in _msg else ''
            )

        elif _msg['command'] == "set_test_config":
            agent_name = None
            if 'agent_name' in _msg:
                agent_name = _msg['agent_name']

            msg = SetTestConfigMessage(_msg['simulate_client_number'],
                                        _msg['client_name'],
                                        _msg['test_type'],
                                        _msg['time'] if 'time' in _msg else 0,
                                        _msg['parameters'] if 'parameters' in _msg else 0,
                                        _msg['test_methods'],
            )
            msg.agent_name= agent_name
            return msg

        elif _msg['command'] == "error":
            return ErrorMessage(_msg['agent_name'], _msg['error_message'])

        elif _msg['command'] == "message":
            return InfoMessage(_msg['agent_name'], _msg['message'])

        elif _msg['command'] == "statistics":
            return StatisticsMessage(_msg['agent_name'], _msg['statistics'])

        elif _msg['command'] == "started":
            return Message(Message.STARTED)

        elif _msg['command'] == "stopped":
            return StoppedMessage(_msg['agent_name'])

        elif _msg['command'] == "get_connected_agents":
            agents  = None
            if 'agents' in _msg:
                agents = _msg['agents']
            return GetConnectedAgentsMessage(agents)
        else:
            return Message(Message.NONE)


if __name__ == '__main__':
    data = [('a', '127.0.0.1'), ('b', '127.0.0.2')]
    msg = GetConnectedAgentsMessage(data)
    print(msg.toJson())
