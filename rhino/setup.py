from setuptools import setup
setup(
    name = "Rhino",
    packages = ["rhino",
    "rhino.agents",
    "rhino.clients",
    "rhino.msg",
    "rhino.network",
    "rhino.pool",
    "rhino.util"],
    version = "0.0.5",
    description = "Framework for stability, stress and performance tests",
    author = "Ferenc Tollas",
    author_email = "ferenc.tollas@gmail.com",
    url = "",
    download_url = "",
    keywords = ["framework", "performance test", "stress test", "stability test"],
    install_requires=['msgpack-python'],
    classifiers = [
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Development Status :: 4 - Beta",
        "Environment :: Other Environment",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries :: Python Modules",
        ],
    long_description = """\
Simple framework for performance and stability tests
-------------------------------------

This version requires Python 3.2 or later;
""",
    entry_points = {
	'rhino.clients': ['exampleclient=rhino.clients.exampleclient:ExampleClient','memory_cpu_client=rhino.clients.memorycpuclient:MemoryCPUMeasureClient']
	}
)
